var simplenodesqllauncher = angular.module('simplenodesqllauncher');

simplenodesqllauncher.controller(
    'LogsController', ['$scope', '$state', 'Api', LogsController]
);

function LogsController($scope, $state, Api) {
    $scope.logs = [];
    $scope.search = '';

    Api.get('/logs').then(
        function (response) {
            $scope.logs = response.data.result;
        },
        function (err) {
            alert('API error !');
            console.log(err);
        }
    );

    $scope.updateLogs = function () {
        Api.get('/logs', {search: $scope.search}).then(
            function (response) {
                $scope.logs = response.data.result;
            },
            function (err) {
                alert('API error !');
                console.log(err);
            }
        );
    };
}
