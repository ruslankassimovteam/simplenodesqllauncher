var simplenodesqllauncher = angular.module('simplenodesqllauncher');

simplenodesqllauncher.controller(
    'QueriesController', ['$scope', '$state', 'Api', QueriesController]
);

function QueriesController($scope, $state, Api) {
    $scope.queries = [];
    $scope.colClass = 'col-4';

    Api.get('/query').then(
        function (response) {
            $scope.queries = response.data.result;
        },
        function (err) {
            alert('API error !');
            console.log(err);
        }
    );

    $scope.executeQuery = function (query) {
        Api.get('/query/' + query).then(
            function (response) {
                $scope.result = response.data.result;

                if ($scope.result.fields.length == 4) {
                    $scope.colClass = 'col-4';
                } else {
                    $scope.colClass = 'col-2';
                }
            },
            function (err) {
                alert('API error !');
                console.log(err);
            }
        );
    };
}
