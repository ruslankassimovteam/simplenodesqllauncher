var simplenodesqllauncher = angular.module('simplenodesqllauncher');

simplenodesqllauncher.factory(
    'Api', ['$http', 'Configuration', ApiService]
);

function ApiService($http, Configuration) {
    return {
        get: function (url, data) {
            var req = {
                method: 'GET',
                url: Configuration.api.url + url,
                headers: {},
                params: data
            };

            return $http(req);
        },
        post: function (url, data) {
            var req = {
                method: 'POST',
                url: Configuration.api.url + url,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };

            return $http(req);
        }
    };
}