simplenodesqllauncher.config(
    ['$urlRouterProvider', '$stateProvider',
        function config($urlRouterProvider, $stateProvider) {
            $stateProvider
                .state(
                    'logs', {
                        url: '/logs',
                        controller: 'LogsController',
                        templateUrl: '/templates/logs.html'
                    }
                )
                .state(
                    'queries', {
                        url: '/queries',
                        controller: 'QueriesController',
                        templateUrl: '/templates/queries.html'
                    }
                )
            ;

            $urlRouterProvider.otherwise('/queries');
        }
    ]
);