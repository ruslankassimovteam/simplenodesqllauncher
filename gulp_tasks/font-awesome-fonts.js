module.exports = function (gulp, plugins, globalConfiguration, moduleConfiguration) {
    return function () {
        gulp.src(globalConfiguration.bower_path + '/components-font-awesome/fonts/**.*')
            .pipe(gulp.dest(moduleConfiguration.fonts_output_path));
    }
}
;