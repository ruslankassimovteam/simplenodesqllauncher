'use strict';

var express = require('express');
var app = express();

console.log('Loading configuration ...');
var configuration = require('./application.configuration.js');

console.log('Booting ...');
var bootstrap = require('./application.bootstrap.js');
bootstrap(app, configuration);

console.log('Loading routes ...');
require('./application.routes.js')(app);

console.log('Connecting to database ...');
var db = require('./services/database/MySql.js');
db.initialize(
    function (database) {
        console.log('Starting server ...');
        app.listen(
            configuration.port,
            function () {
                console.log(
                    'Application launched on port ' + configuration.port + '.\n------------------------\n'
                );
            }
        );
    }
);

