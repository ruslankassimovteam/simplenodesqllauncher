'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var Response = require('./services/app/Response.js');
var HomeController = require('./controllers/HomeController.js');
var QueryController = require('./controllers/QueryController.js');
var LogController = require('./controllers/LogController.js');

var ROUTING = function (app) {

    app.use(cors());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    // ---------------------- API ----------------------
    app.get(
        '/api/',
        HomeController.indexAction
    );

    app.get(
        '/api/query/:queryName?',
        QueryController.queryAction
    );

    app.get(
        '/api/logs',
        LogController.logsAction
    );

    // --------------------- STATIC ---------------------

    app.use(express.static(__dirname + '/../public'));

    // ---------------------- 404 ----------------------
    app.post(
        '*',
        function (req, res) {
            res.status(404);
            res.json((new Response(null, ['Not found'])));
        }
    );
};

module.exports = ROUTING;
