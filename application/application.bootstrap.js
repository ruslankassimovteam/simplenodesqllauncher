'use strict';

var BOOTSTRAP = function (app, configuration) {
    console.logDebug = function (text) {
        if (configuration.debug) {
            console.log(text);
        }
    }
};

module.exports = BOOTSTRAP;