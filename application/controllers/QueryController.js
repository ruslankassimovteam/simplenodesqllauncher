'use strict';

var fs = require('fs');
var _ = require('underscore');
var configuration = require('../application.configuration.js');
var db = require('../services/database/MySql.js');
var logger = require('../services/logs/Logger.js');

var Response = require('./../services/app/Response.js');

var CONTROLLER = function () {
    var self = this;

    self.queryAction = function (request, response) {

        var queryName = request.params.queryName;
        var result = null;
        var errors = [];

        if (!queryName) {
            fs.readdir(
                configuration.queriesFolder,
                function (err, files) {
                    if (err) {
                        console.log(err);
                        errors.push('Folder unavailable');
                        response.status(500);
                    } else {
                        result = _.filter(
                            files,
                            function (fileName) {
                                return (/\.sql$/i).test(fileName);
                            }
                        );
                        result = _.map(
                            result,
                            function (fileName) {
                                return fileName.replace(/\.[^/.]+$/, '');
                            }
                        );
                    }

                    response.json(new Response(result, errors));
                }
            );
        } else {
            var queryPath = configuration.queriesFolder + '/' + queryName + '.sql';
            if (fs.existsSync(queryPath)) {
                var query = fs.readFileSync(queryPath, 'utf8');
                var log = 'QUERY : \n\n ' + query + '\n\n';

                db.connection.query(
                    query,
                    function (err, rows, fields) {
                        if (!err) {
                            if (_.isArray(rows)) {
                                log += 'RESULT : \n\n' + _.map(
                                        rows,
                                        function (row) {
                                            return _.pairs(row).join(';');
                                        }
                                    ).join('\n');

                                var cleanedFields = _.map(
                                    fields,
                                    function (field) {
                                        return field.name;
                                    }
                                );
                                result = {
                                    fields: cleanedFields,
                                    rows: rows
                                };
                            } else {
                                log += 'RESULT : \n\n affectedRows: ' + rows.affectedRows + 'changedRows: ' + rows.changedRows;

                                result = {
                                    affectedRows: rows.affectedRows,
                                    changedRows: rows.changedRows
                                }
                            }
                        } else {
                            log += 'ERROR : ' + err.message;
                            console.log(err);
                            errors.push('Error during query execution');
                            response.status(500);
                        }

                        logger.log(log + '\n\n');
                        response.json(new Response(result, errors));
                    }
                );
            } else {
                errors.push('Query unavailable');
                response.status(500);
                response.json(new Response(result, errors));
            }
        }
    };
};

module.exports = new CONTROLLER();
