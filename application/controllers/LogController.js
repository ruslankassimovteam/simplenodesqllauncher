'use strict';

var fs = require('fs');
var _ = require('underscore');
var configuration = require('../application.configuration.js');

var Response = require('./../services/app/Response.js');

var CONTROLLER = function () {
    var self = this;

    self.logsAction = function (request, response) {
        fs.readdir(
            configuration.logsFolder,
            function (err, files) {
                var result = null;
                var errors = [];

                if (err) {
                    console.log(err);
                    errors.push('Folder unavailable');
                    response.status(500);
                } else {
                    files = _.without(files, '.gitkeep');

                    result = _.map(
                        files,
                        function (fileName) {
                            var logPath = configuration.logsFolder + '/' + fileName;
                            return {
                                fileName : fileName,
                                content : (fs.existsSync(logPath) ? fs.readFileSync(logPath, 'utf8') : null)
                            };
                        }
                    );

                    if (request.query.search && request.query.search != '') {
                        result =_.filter(
                            result,
                            function (log) {
                                return log.content.indexOf(request.query.search) !== -1;
                            }
                        );
                    }
                }

                response.json(new Response(result, errors));
            }
        );
    };
};

module.exports = new CONTROLLER();
