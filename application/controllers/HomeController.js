'use strict';

var Response = require('./../services/app/Response.js');

var CONTROLLER = function () {
    var self = this;

    self.indexAction = function (request, response) {
        response.status(418);
        response.json(new Response('I\'m a teapot'));
    };
};

module.exports = new CONTROLLER();
