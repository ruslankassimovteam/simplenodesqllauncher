"use strict";

var configuration = require('./../../application.configuration.js');
var fs = require('fs');
var _ = require('underscore');

var LOGGER = function () {
    var self = this;

    self.log = function (text) {
        self.getLogFile(
            function (filePath, fileContent) {
                var fileContentLength = fileContent.length;
                var textLength = text.length;
                var overflowText = '';

                if (fileContentLength + textLength > 1024) {
                    var subtstrLength = 1024 - fileContentLength;
                    overflowText = text.substr(subtstrLength, text.length);
                    text = text.substr(0, subtstrLength);
                }

                try {
                    fs.writeFileSync(filePath, fileContent + text, 'ascii');
                } catch (e) {
                    console.log('ERROR : Unable to write log file :');
                    console.log(e.message);
                }

                if (overflowText.length > 0) {
                    self.log(overflowText);
                }
            }
        );
    };

    self.getLogFile = function (cb) {
        fs.readdir(
            configuration.logsFolder,
            function (err, files) {
                if (err) {
                    console.log('ERROR : Unable to access logs folder :');
                    console.log(err);
                } else {
                    var maxLogsIndex = 0;
                    files = _.without(files, '.gitkeep');

                    if (files.length > 0) {
                        var logsIndexes = _.map(
                            files,
                            function (fileName) {
                                return parseInt(fileName.split('-')[1]);
                            }
                        );
                        maxLogsIndex = _.max(logsIndexes);
                    }

                    var newLogPath = configuration.logsFolder + '/query-' + maxLogsIndex + '.log';

                    var fileContent = '';
                    if (files.length > 0) {
                        fileContent = fs.readFileSync(newLogPath, 'ascii');
                    }

                    if (fileContent.length >= 1024) {
                        newLogPath = configuration.logsFolder + '/query-' + (maxLogsIndex + 1) + '.log';
                        fileContent = '';
                    }


                    cb(
                        newLogPath,
                        fileContent
                    );
                }
            }
        );
    }
};

module.exports = new LOGGER();