"use strict";

var mysql = require('mysql');
var configuration = require('./../../application.configuration.js');

var MYSQL = function () {
    var self = this;
    self.connection = null;

    self.initialize = function (cb) {
        var connection = mysql.createConnection({
            host: configuration.host,
            user: configuration.user,
            password: configuration.password,
            database: configuration.database
        });
        connection.connect(function (err) {
            if (err) throw err;
            console.log('Connected to database : ' + configuration.database);
            cb(this);
        });
        self.connection = connection;
    };
};

module.exports = new MYSQL();