"use strict";

var RESPONSE = function (result, errors) {
    this.result = result || null;
    this.errors = errors || [];
};

module.exports = RESPONSE;