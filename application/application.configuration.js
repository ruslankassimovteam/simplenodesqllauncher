function CONFIGURATION() {
    var c = {};

    c.port = 8080;
    c.debug = true;

    c.queriesFolder = __dirname + '/../assets/sql';
    c.logsFolder = __dirname + '/../assets/logs';

    c.host = '127.0.0.1';
    c.user = 'root';
    c.password = '';
    c.database = 'simple_node_sql_launcher';

    return c;
}

module.exports = CONFIGURATION();