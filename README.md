SimpleNodeSqlLauncher
==========

# Installation

Proceed to installation :
```bash
npm install
bower install
gulp
```

Create `simple_node_sql_launcher` database using `database.sql` file.

Update `application/application.configuration.js` to reflect your database settings.

Eventually, update the API url in `front-application/application.configuration.js`.

Finally, launch with :
```bash
node application/application.js
```