'use strict';

var configuration = {
    // Global Gulp configuration
    gulp_tasks_path: 'gulp_tasks',
    bower_path: 'bower_components',

    modules: {},

    addModule: function (name, cb) {
        var module = {};
        this.modules[name] = cb(module);
    }
};

configuration.addModule(
    'main',
    function (m) {
        // SASS configuration
        m.sass_root_path = './assets/scss';
        m.sass_main_file = 'style.scss';
        m.sass_output_path = './public/assets/css';
        m.sass_output_name = 'main';

        // JS configuration
        m.js_root_path = './front-application';
        m.js_include_order = [
            configuration.bower_path + '/angular/angular.js',
            configuration.bower_path + '/angular-ui-router/release/angular-ui-router.js',
            m.js_root_path + '/application.js',
            m.js_root_path + '/application.configuration.js',
            m.js_root_path + '/application.routes.js',
            m.js_root_path + '/services/**/*.js',
            m.js_root_path + '/controllers/**/*.js'
        ];
        m.js_output_path = './public/assets/js';
        m.js_output_name = 'main';

        // Fonts configuration
        m.fonts_output_path = './public/assets/fonts';

        return m;
    }
);

module.exports = configuration;